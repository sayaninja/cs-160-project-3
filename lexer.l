%option yylineno
%pointer

%{
    #include <cstdlib>
    #include <cerrno>
    #include <climits>
    #include <limits>
    #include "parser.hpp"
    
	void yyerror(const char *);
%}


digit [0-9]
%x IN_COMMENT
%%
<INITIAL>{
 "--!"              BEGIN(IN_COMMENT);
}
<IN_COMMENT>{
 "!--"      BEGIN(INITIAL);
 [^!]+      // eat comment in chunks
 "!"        // eat the lone star
 <<EOF>> 	{ yyerror("dangling comment"); }
}



{digit}+    {return T_NUMBER;}
"+"         {return T_PLUS;}
"-"         {return T_MINUS;}
"*"         {return T_MULTIPLY;}
"/"         {return T_DIVIDE;}
"<"         {return T_LESS;}
"<="        {return T_LESSOREQUAL;}
"="         {return T_EQUAL;}
"and"       {return T_AND;}
"or"        {return T_OR;}
"not"       {return T_NOT;}
"print"     {return T_PRINT;}
"return"    {return T_RETURN;}
"if"        {return T_IF;}
"else"      {return T_ELSE;}
"equals"    {return T_EQUALS;}
"while"     {return T_WHILE;}
"extends"   {return T_EXTENDS;}
"boolean"   {return T_BOOL;}
"true"      {return T_TRUE;}
"false"     {return T_FALSE;}
"integer"   {return T_INT;}
"new"       {return T_NEW;}
"none"		{return T_NONE;}
"("         {return T_OPENPAREN;}
")"         {return T_CLOSEPAREN;}
"{"         {return T_OPENBRACK;}
"}"         {return T_CLOSEBRACK;}
","         {return T_COMMA;}
"->"        {return T_ARROW;}
":"         {return T_COLON;}
"."         {return T_DOT;} 
[a-zA-Z][a-zA-Z0-9]* {return T_ID;}
[ \t\n]     ;

.           { yyerror("invalid character"); }

%%

int yywrap(void) {
  return 1;
}
