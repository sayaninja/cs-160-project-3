/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_NUMBER = 258,
     T_ID = 259,
     T_INT = 260,
     T_PLUS = 261,
     T_MINUS = 262,
     T_MULTIPLY = 263,
     T_DIVIDE = 264,
     T_LESS = 265,
     T_LESSOREQUAL = 266,
     T_EQUAL = 267,
     T_EQUALS = 268,
     T_AND = 269,
     T_OR = 270,
     T_NOT = 271,
     T_PRINT = 272,
     T_RETURN = 273,
     T_IF = 274,
     T_ELSE = 275,
     T_WHILE = 276,
     T_OPENPAREN = 277,
     T_CLOSEPAREN = 278,
     T_OPENBRACK = 279,
     T_CLOSEBRACK = 280,
     T_EXTENDS = 281,
     T_NEW = 282,
     T_NONE = 283,
     T_BOOL = 284,
     T_FALSE = 285,
     T_TRUE = 286,
     T_COLON = 287,
     T_DOT = 288,
     T_COMMA = 289,
     T_ARROW = 290,
     U_MINUS = 291
   };
#endif
/* Tokens.  */
#define T_NUMBER 258
#define T_ID 259
#define T_INT 260
#define T_PLUS 261
#define T_MINUS 262
#define T_MULTIPLY 263
#define T_DIVIDE 264
#define T_LESS 265
#define T_LESSOREQUAL 266
#define T_EQUAL 267
#define T_EQUALS 268
#define T_AND 269
#define T_OR 270
#define T_NOT 271
#define T_PRINT 272
#define T_RETURN 273
#define T_IF 274
#define T_ELSE 275
#define T_WHILE 276
#define T_OPENPAREN 277
#define T_CLOSEPAREN 278
#define T_OPENBRACK 279
#define T_CLOSEBRACK 280
#define T_EXTENDS 281
#define T_NEW 282
#define T_NONE 283
#define T_BOOL 284
#define T_FALSE 285
#define T_TRUE 286
#define T_COLON 287
#define T_DOT 288
#define T_COMMA 289
#define T_ARROW 290
#define U_MINUS 291




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

