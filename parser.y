%{
    #include <cstdlib>
    #include <cstdio>
    #include <iostream>

    #define YYDEBUG 1

    int yylex(void);
    void yyerror(const char *);
%}

%error-verbose

/* List all your tokens here */

%token T_NUMBER T_ID T_INT
%token T_PLUS T_MINUS 
%token T_MULTIPLY T_DIVIDE 
%token T_LESS T_LESSOREQUAL 
%token T_EQUAL T_EQUALS 
%token T_AND T_OR T_NOT
%token T_PRINT T_RETURN 
%token T_IF T_ELSE T_WHILE 
%token T_OPENPAREN T_CLOSEPAREN 
%token T_OPENBRACK T_CLOSEBRACK 
%token T_EXTENDS T_NEW T_NONE
%token T_BOOL T_FALSE T_TRUE 
%token T_COLON T_DOT T_COMMA T_ARROW

/* Specify precedence here */

%left T_OR
%left T_AND
%left T_LESS T_LESSOREQUAL T_EQUALS
%left T_PLUS T_MINUS
%left T_MULTIPLY T_DIVIDE
%right T_NOT U_MINUS


%%

/* Grammar  */

Classes     : Class Classes
            | Class
            ;
Class       : T_ID T_EXTENDS T_ID T_OPENBRACK Members T_CLOSEBRACK
            | T_ID T_OPENBRACK Members T_CLOSEBRACK 
            ;  


Members     : Type T_ID Members
            | Methods
            ;

Type        : T_BOOL
            | T_INT
            | T_ID 
            ;     

ReturnType  : T_BOOL
            | T_INT
            | T_ID 
            | T_NONE
            ;     

Methods     : T_ID T_OPENPAREN Parameters T_CLOSEPAREN T_ARROW ReturnType T_OPENBRACK Declarations T_CLOSEBRACK Methods
            |
            ;

Parameters  : ParametersP
            | 
            ;

ParametersP : ParametersP T_COMMA T_ID T_COLON Type
            | T_ID T_COLON Type
            ;

Declarations: Declaration Declarations 
            | Statements
            ;

Declaration : Declaration T_COMMA T_ID
            | Type T_ID 
            ;

Statements  : Statement Statements   
            | Return  
            ;

Return      : T_RETURN Expression
            |
            ;

Statement   : Assignment
            | MethodCall
            | IfElse
            | While 
            | Print
            ;

Assignment  : T_ID T_EQUAL Expression
            | T_ID T_DOT T_ID T_EQUAL Expression
            ;

MethodCall  : T_ID T_OPENPAREN Arguments T_CLOSEPAREN
            | T_ID T_DOT T_ID T_OPENPAREN Arguments T_CLOSEPAREN

IfElse      : T_IF Expression T_OPENBRACK Block T_CLOSEBRACK
            | T_IF Expression T_OPENBRACK Block T_CLOSEBRACK T_ELSE T_OPENBRACK Block T_CLOSEBRACK
            ;

While       : T_WHILE Expression T_OPENBRACK Block T_CLOSEBRACK
            ;

Block       : Block Statement
            | Statement
            ;

Print       : T_PRINT Expression
            ;

Expression  : Expression T_PLUS Expression
            | Expression T_MINUS Expression
            | Expression T_MULTIPLY Expression
            | Expression T_DIVIDE Expression
            | Expression T_LESS Expression
            | Expression T_LESSOREQUAL Expression
            | Expression T_EQUALS Expression
            | Expression T_AND Expression
            | Expression T_OR Expression
            | T_NOT Expression
            | T_MINUS Expression %prec U_MINUS
            | T_ID
            | T_ID T_DOT T_ID         
            | MethodCall                                  
            | T_OPENPAREN Expression T_CLOSEPAREN
            | T_NUMBER
            | T_TRUE 
            | T_FALSE
            | T_NEW T_ID
            | T_NEW T_ID T_OPENPAREN Arguments T_CLOSEPAREN
            ;


Arguments   : ArgumentsP
            | 
            ;

ArgumentsP  : ArgumentsP T_COMMA Expression
            | Expression
            ;


%%

extern int yylineno;

void yyerror(const char *s) {
  fprintf(stderr, "%s at line %d\n", s, yylineno);
  exit(1);
}
